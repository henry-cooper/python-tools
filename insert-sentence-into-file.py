# TBD
# 从插入行改为插入到某个固定key的下一行   找到指定key的行数，然后在下一行插入
# 修改
# 删除
# Add new feature: step1 ~ step3
# Use this program: step4 ~ step6

from collections import OrderedDict
from enum import Enum
import sys
import glob
import json

class Command(Enum):
    ModifySubString = 0
    AddItem = 1
    DeleteItem = 2
    #step1: Add command here

condition = {}

def modifySubString(contents):
    for sectionKey in contents:
        sectionContents = contents[sectionKey]
        for itemKey in sectionContents:
            if modifySubStringConfig["targetString"] in sectionContents[itemKey]:
                print(sectionContents[itemKey])
                sectionContents[itemKey] = sectionContents[itemKey].replace(modifySubStringConfig["targetString"], modifySubStringConfig["replaceString"])
                print(sectionContents[itemKey])

def addItem(contents):
    return

def deleteItem(contents):
    for sectionKey in contents:
        if deleteItemConfig["sectionKey"] != sectionKey:
            continue
        sectionContents = contents[sectionKey]
        for itemKey in sectionContents:
            if deleteItemConfig["itemKey"] == itemKey:
                contents[sectionKey].pop(itemKey)
                return

# def change(fileName, condition):
#     modifyType = condition["modifyType"]
#     firstKey = condition["firstKey"]
#     subKey = condition["subKey"]
#     line = condition["line"]
#     changeValue = condition["changeValue"]    
#     addKey = condition["addKey"]
#     addValue = condition["addValue"]

#     f = open(fileName, encoding="utf-8-sig")
#     contents = json.load(f,object_pairs_hook=OrderedDict)
#     for tmpFirstKey in contents:
#         if tmpFirstKey == firstKey:
#             eachContents = contents[tmpFirstKey]
#             if modifyType == "insertLast":
#                 eachContents[addKey] = addValue          
#             else:
#                 for tmpSubkey in eachContents:
#                     if tmpSubkey == subKey:
#                         if modifyType == "change":
#                             eachContents[subKey] = changeValue
#                         elif modifyType == "delete":
#                             contents[tmpFirstKey].pop(subKey)
               
#     f.close()
#     fw = open(fileName, "w", encoding="utf-8-sig")
#     json.dump(contents, fw, indent=4, ensure_ascii=False)
#     fw.close()                

def openFile(fileName):
    f = open(fileName, encoding="utf-8-sig")
    contents = json.load(f,object_pairs_hook=OrderedDict)
    return (f, contents)

def saveFile(fileHandle, fileName, contents):
    fileHandle.close()
    fw = open(fileName, "w", encoding="utf-8-sig")
    json.dump(contents, fw, indent=4, ensure_ascii=False)
    fw.close()    

def mainProcess(command, condition):
    allFileName = glob.glob(condition['path'] + "/*." + condition['fileType'])
    for name in allFileName:
        file = openFile(name)
        dispatcher[command](file[1])
        saveFile(file[0], name, file[1])

dispatcher = {
    Command.ModifySubString: modifySubString,
    Command.AddItem: addItem,
    Command.DeleteItem: deleteItem
    # step2: add the handle function for command
    # Command.XXXX: handler
}

modifySubStringConfig = {
    "targetString": "data-uap-navigate=\"ms-settings:windowsdefender\" href=\"javascript:void(0);\"",
    "replaceString": "ABC"
}

deleteItemConfig = {
    "sectionKey": "ov",
    "itemKey": "fingerprint-reg"
}

# step3: add the command config keys

if __name__ == "__main__":
    # step4 : modify the folder path if needed
    condition["path"] = "E:\\svn\\Vantage\\branches\\Components.SecurityCenter\\Assets\\LocalHtmls\\json"
    condition["fileType"] = "json"
    # step5 : modify the command
    # step6 : modify the value in step3
    mainProcess(Command.ModifySubString, condition)