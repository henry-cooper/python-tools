import json
import urllib.request
from enum import Enum
import random, os
import base64
import glob
import string

class ModelType(Enum):
    Promotion = 0
    Navigation = 1
    Bento = 2
    Herobanner = 3

baseUrl = "http://localhost:3000"

bentoUrl = '/api/bentos'
navigationUrl = '/api/navigations'
promotionUrl = '/api/promotions'
herobannerUrl = '/api/herobanners'

def createPromotion():
  promotion = {}
  promotion['id'] = random.randint(1,10000)
  promotion['image'] = getRandomImage()
  promotion['title'] = randomword(15)
  promotion['description'] = randomword(40)
  promotion['linkText'] = randomword(10)
  promotion['link'] = r'https://angular.io/docs'
  
  return promotion

def createNavigation():
  navigation = {}
  navigation['id'] = random.randint(1,10000)
  navigation['icon'] = getRandomImage()
  navigation['title'] = randomword(15)
  navigation['linkURL'] = r'https://angular.io/docs'
  
  return navigation

def createBento():
  bento = {}
  bento['id'] = random.randint(1,10000)
  bento['icon'] = getRandomImage()
  bento['text'] = randomword(15)
  bento['linkURL'] = r'https://angular.io/docs'
  
  return bento

def createHeroBanner():
  banner = {}
  banner['id'] = random.randint(1,10000)
  banner['image'] = getRandomImage()
  banner['title'] = randomword(15)
  banner['description'] = randomword(40)
  banner['linkText'] = randomword(10)
  banner['link'] = r'https://angular.io/docs'
  
  return banner

def getRandomImage():
  return encode(search(os.path.dirname(os.path.realpath(__file__)) + '\\img'))

def search(path):
    random_filename = random.choice([
        x for x in os.listdir(path)
        if os.path.isfile(os.path.join(path, x))
    ])
    return os.path.join(path, random_filename)

def encode(path):
    with open(path, 'rb') as image:
        image_read = image.read()
        image_64_encode = base64.b64encode(image_read)
        return image_64_encode.decode("utf-8")

def randomword(length):
   letters = string.ascii_lowercase
   return ''.join(random.choice(letters) for i in range(length))

def postRequest(body, url):
  req = urllib.request.Request(url)
  req.add_header('Content-Type', 'application/json; charset=utf-8')

  jsondata = body
  jsondataasbytes = jsondata.encode('utf-8')   # needs to be bytes
  req.add_header('Content-Length', len(jsondataasbytes))
  print (jsondataasbytes)
  response = urllib.request.urlopen(req, jsondataasbytes)
  print (response)

def createMockData(num, type, url):
  for _ in range(num):
    item = getRandomModel(type)
    postRequest(json.dumps(item), baseUrl + url)

def getRandomModel(type):
  return modelMap[type]()

modelMap = {
    ModelType.Promotion: createPromotion,
    ModelType.Navigation: createNavigation,
    ModelType.Bento: createBento,
    ModelType.Herobanner: createHeroBanner
}

if __name__ == "__main__":
  createMockData(3, ModelType.Bento, bentoUrl)
  createMockData(5, ModelType.Herobanner, herobannerUrl)
  createMockData(2, ModelType.Navigation, navigationUrl)
  createMockData(4, ModelType.Promotion, promotionUrl)